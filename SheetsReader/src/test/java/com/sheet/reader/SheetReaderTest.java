package com.sheet.reader;

import static org.junit.Assert.assertTrue;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

public class SheetReaderTest {

	List<ArrayList<Object>> values;

	@Before
	public void setUp() throws Exception {
		values = new ArrayList<ArrayList<Object>>();
		InputStream in = GoogleSheetReader.class.getResourceAsStream("/responseData.json");
		Object[] spreadSheetValueArray = IOUtils.readLines(in, "UTF-8").toArray();
		ArrayList<Object> responseList = new ArrayList<Object>(Arrays.asList(spreadSheetValueArray));
		values = Arrays.asList(responseList);
		System.out.println(values.toString());
	}

	@Test
	public void test_OutputFileParameter_ReturnTrue() {
		boolean expected = values.get(0).toString().contains("STATUS");
		assertTrue(expected);
	}

	@Test
	public void test_OutputFileParameter_ReturnFalse() {
		boolean expected = values.get(0).toString().contains("XYZ");
		assertTrue(expected);
	}

	@Test(expected = FileNotFoundException.class)
	public void testReadFile_ReturnTrue() throws IOException {
		FileReader reader = new FileReader("/" + FileInfo.CREDENTIALS_FILE_PATH.getValue());
		reader.read();
		reader.close();
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void test_CheckActualCostIndexOutOfBoundException_ReturnTrue() {
		values.get(0).get(8);
	}

	@Test
	public void callPrivateGoogleSheetReaderMethod() throws NoSuchMethodException, SecurityException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		GoogleSheetReader readerSheet = new GoogleSheetReader();
		Method privateMethod = GoogleSheetReader.class.getDeclaredMethod("printTotalActualHourAndFixedCost",
				List.class);
		privateMethod.setAccessible(true);
		privateMethod.invoke(readerSheet, values);
	}
}
