package com.sheet.reader.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.sheet.reader.FileInfo;
import com.sheet.reader.GoogleSheetReader;

public class GoogleUtil {
	/**
	 * Creates an authorized Credential object.
	 * 
	 * @param HTTP_TRANSPORT The network HTTP Transport.
	 * @return An authorized Credential object.
	 * @throws IOException If the credentials.json file cannot be found.
	 */
	public static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
		// Load client secrets.
		InputStream in = GoogleSheetReader.class.getResourceAsStream(FileInfo.CREDENTIALS_FILE_PATH.getValue());
		if (in == null) {
			throw new FileNotFoundException("Resource not found: " + FileInfo.CREDENTIALS_FILE_PATH.getValue());
		}
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(GoogleSheetReader.JSON_FACTORY,
				new InputStreamReader(in));
		// Build flow and trigger user authorization request.
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT,
				GoogleSheetReader.JSON_FACTORY, clientSecrets, GoogleSheetReader.SCOPES)
						.setDataStoreFactory(
								new FileDataStoreFactory(new java.io.File(FileInfo.TOKENS_DIRECTORY_PATH.getValue())))
						.setAccessType("offline").build();
		LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
		return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
	}
}
