package com.sheet.reader;

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.sheet.reader.util.GoogleUtil;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.List;

public class GoogleSheetReader {

	public static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	/**
	 * Global instance of the scopes required by this sheetReader. If modifying these
	 * scopes, delete your previously saved tokens/ folder.
	 */
	public static final List<String> SCOPES = Collections.singletonList(SheetsScopes.SPREADSHEETS_READONLY);

	/**
	 * Reads the data and prints on the console:
	 * https://docs.google.com/spreadsheets/d/1OvtGTEDJi_j2SEVegNlJXtydg6_EyfRSJB40kNJI2ds/edit
	 */
	public static void main(String... args) throws IOException, GeneralSecurityException {
		// Build a new authorized API client service.
		final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
		final String spreadsheetId = "1OvtGTEDJi_j2SEVegNlJXtydg6_EyfRSJB40kNJI2ds";
		final String range = "Sheet1!A1:K6";

		Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, GoogleUtil.getCredentials(HTTP_TRANSPORT))
				.setApplicationName(FileInfo.APPLICATION_NAME.getValue()).build();
		ValueRange response = service.spreadsheets().values().get(spreadsheetId, range).execute();
		List<List<Object>> values = response.getValues();
		if (values == null || values.isEmpty()) {
			System.out.println("No data found.");
		} else {
			printData(values);
		}
	}

	/**
	 * @description - method to print the data retrieved from a Google Sheet on the console
	 * @param values - response received from the API to read Google Sheet
	 * @return void - since it just prints the data on console
	 * */
	private static void printData(List<List<Object>> values) {
		// TODO Auto-generated method stub
		boolean printOnce = true;
		GoogleSheetReader sheetReader = new GoogleSheetReader();
		Double fixedCostTotal = 0d;
		Integer actualHoursTotal = 0;
		for (int i = 2; i < values.size(); i++) {
			if (printOnce) {
				System.out.println("\n");
				System.out.println("Status | Priority | Deadline | Fixed Cost | Actual Hrs");
				printOnce = false;
			}
			sheetReader.printDetailsPerRecord(values.get(i));
			if(!values.get(i).isEmpty()) {
				if(values.get(i).get(8)!=null) {
					fixedCostTotal += Double.parseDouble(values.get(i).get(8).toString().substring(1));
				}
				if(values.get(i).get(10)!=null) {
					actualHoursTotal += Integer.parseInt(values.get(i).get(10).toString());
				}
			}
		}
		sheetReader.printTotalActualHourAndFixedCost(fixedCostTotal, actualHoursTotal);
	}

	/**
	 * @description - method to print the Fixed Cost & Actual Hours
	 * @param value - element at the 1st location of the sheet response object
	 * @return void - since it just prints the data on console
	 * */
	private void printTotalActualHourAndFixedCost(Double fixedCostTotal, Integer actualHoursTotal) {
		System.out.println("\nFixed Cost - $" + fixedCostTotal);
		System.out.println("Actual Hrs - " + actualHoursTotal);
	}

	/**
	 * @description - method to print the Status, Priority, Deadline, Fixed Cost, Actual Hrs
	 * @param value - elements starting from the 2nd location of the sheet response object
	 * @return void - since it just prints the data on console
	 * */
	private void printDetailsPerRecord(List<Object> values) {
		System.out.printf("%s | %s | %s | %s | %s\n", values.get(0), values.get(1),
				values.get(2), values.get(8), values.get(10));
	}
}
