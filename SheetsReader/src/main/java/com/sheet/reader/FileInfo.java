package com.sheet.reader;

public enum FileInfo {

	APPLICATION_NAME  ("Google Sheets API Java Quickstart"),  //calls constructor
	TOKENS_DIRECTORY_PATH("tokens"),  
	CREDENTIALS_FILE_PATH   ("/credentials.json") ; // semicolon needed when fields / methods follow

    private final String value;

    private FileInfo(String value) {
        this.value = value;
    }
    
    public String getValue() {
        return this.value;
    }
}
